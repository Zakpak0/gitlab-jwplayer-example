import React, { useEffect } from "react";
import useWindow from "./useWindow";

export default function useHost() {
    const window = useWindow() as globalThis.Window;
    const [hostAddress, setHost] = React.useState<string>(null as any);
    useEffect(() => {
        if (window) {
            setHost(window.location.host);
        }
    }, [window]);
    const host = hostAddress
        ? `${process.env.NODE_ENV == "development" ? "http" : "https"}://${hostAddress}`
        : null;
    return host;
}
