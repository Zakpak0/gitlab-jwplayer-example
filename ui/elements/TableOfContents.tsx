import Link from "next/link";

export default function TableOfContents() {
    const content = [
        {
            title: "Introduction",
            href: "/",
        },
        {
            title: "Basic Player Implementation",
            href: "/basic",
            pages: [
                {
                    title: "Basic Player",
                    href: "/basic/player",
                },
                {
                    title: "Responsive Player",
                    href: "/basic/responsive-player",
                },
                {
                    title: "Fixed Dimensions Player",
                    href: "/basic/fixed-dimension-player",
                },
            ],
        },
        {
            title: "Custom Player Implementation",
            href: "/custom",
            pages: [
                {
                    title: "Custom Icons Player",
                    href: "/custom/icons-player",
                },
                {
                    title: "Custom Text Player",
                    href: "/custom/text-player",
                },
                {
                    title: "Autoplay Player",
                    href: "/custom/autoplay-player",
                },
            ],
        },
        {
            title: "Playlists Player Implementation",
            href: "/playlists",
            pages: [
                {
                    title: "Autoplay Player",
                    href: "/playlists/autoplay-player",
                },
            ],
        },
        {
            title: "Content Management Player Implementation",
            href: "/content-management",
            pages: [
                {
                    title: "Subtitles Player",
                    href: "/content-management/subtitles-player",
                },
            ],
        },
    ];
    function calculatePanelHeight() {
        const size = content
            .map((section) => {
                if (section.pages) {
                    return section.pages.length + 1;
                }
                return 1;
            })
            .reduce((a, b) => a + b);
        return size * 25;
    }
    function calculatePanelWidth() {
        let currentLargest = "Table of Contents".length;
        const largestTitle = content.map((section) => {
            if (section.pages) {
                const pageLengths = section.pages.map((page) => page.title.length);
                return [section.title.length, ...pageLengths];
            }
            return section.title.length;
        });
        for (let title of largestTitle) {
            if (title instanceof Array) {
                for (let subTitle of title) {
                    if (subTitle > currentLargest) {
                        currentLargest = subTitle;
                    }
                }
            } else {
                if (title > currentLargest) {
                    currentLargest = title;
                }
            }
        }
        return currentLargest * 10 + 40;
    }
    return (
        <div
            style={{
                display: "flex",
                flexDirection: "column",
                width: calculatePanelWidth(),
                alignItems: "center",
            }}>
            <h1>Table of Contents</h1>
            <div
                style={{
                    display: "flex",
                    flexDirection: "column",
                    minHeight: calculatePanelHeight(),
                    justifyContent: "space-evenly",
                }}>
                {content.map((section) => {
                    return (
                        <div
                            style={{
                                marginTop: 20,
                            }}
                            key={section.title}>
                            <Link
                                style={{
                                    fontWeight: "bold",
                                }}
                                href={section.href}>
                                {section.title}
                            </Link>
                            {section.pages &&
                                section.pages.map((page) => {
                                    return (
                                        <div
                                            style={{
                                                marginTop: 5,
                                            }}
                                            key={page.title}>
                                            <Link href={page.href}>{page.title}</Link>
                                        </div>
                                    );
                                })}
                        </div>
                    );
                })}
            </div>
        </div>
    );
}
