import Link from "next/link";
import PageWrapper from "../../ui/components/PageWrapper";

export default function ContentManagementTOC() {
    return (
        <PageWrapper>
            <div
                style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                }}>
                <h1>Content Management Player</h1>
                <p>Content Management player example</p>
                <Link href={"/custom/subtitles-player"}>Subtitles Player</Link>{" "}
            </div>
        </PageWrapper>
    );
}
