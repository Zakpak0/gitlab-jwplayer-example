import Link from "next/link";
import PageWrapper from "../../ui/components/PageWrapper";

export default function BasicTOC() {
    return (
        <PageWrapper>
            <div
                style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                }}>
                <h1>Basic Player</h1>
                <p>Basic player example</p>
                <Link href={"/basic/player"}>Basic Player</Link>
                <Link href={"/basic/responsive-player"}>Responsive Player</Link>
                <Link href={"/basic/fixed-dimension-player"}>Fixed Dimensions Player</Link>
            </div>
        </PageWrapper>
    );
}
