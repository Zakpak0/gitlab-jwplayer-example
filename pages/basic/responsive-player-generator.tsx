import Head from "next/head";
import Script from "next/script";
import React from "react";
import JWPlayer from "../../features/jwplayer";
import styles from "../../styles/Home.module.css";

export default function ResponsivePlayerGenerator({ playerId }: { playerId: string }) {
    return (
        <React.Fragment>
            <Script
                src={`https://cdn.jwplayer.com/libraries/${playerId}.js`}
                onReady={() => {
                    const player = jwplayer("responsive-player");
                    player.setup({
                        file: "https://interactive-examples.mdn.mozilla.net/media/cc0-videos/flower.webm",
                        responsive: true,
                        aspectratio: "16:9",
                        skin: {
                            name: "player",
                        },
                        height: "100%",
                        width: "100%",
                    });
                }}
            />

            <div id="responsive-player" />
        </React.Fragment>
    );
}
ResponsivePlayerGenerator.getInitialProps = async () => {
    async function getPlayers() {
        const params = "?page=1&page_length=10&sort=created%3Adsc";
        const query = await JWPlayer.SDK.getPlayers(params);
        return query;
    }
    const players = await getPlayers();
    const playerId = players.players[0].id;
    return { playerId };
};
