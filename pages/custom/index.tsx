import Link from "next/link";
import PageWrapper from "../../ui/components/PageWrapper";

export default function CustomTOC() {
    return (
        <PageWrapper>
            <div
                style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                }}>
                <h1>Custom Player</h1>
                <p>Custom player example</p>
                <Link href={"/custom/icons-player"}>Custom Icons Player</Link>
                <Link href={"/custom/text-player"}>Custom Text Player</Link>
                <Link href={"/custom/autoplay-player"}>Autoplay Player</Link>
            </div>
        </PageWrapper>
    );
}
