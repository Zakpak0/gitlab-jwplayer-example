import React, { useEffect } from "react";

export default function useWindow() {
    const [windowObject, setWindow] = React.useState<globalThis.Window>(null as any);
    useEffect(() => {
        setWindow(window);
    }, []);
    return windowObject;
}
