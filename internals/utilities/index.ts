export default class Utilities {
    static async jwPlayerRequest({
        resource,
        params,
        method,
        body,
        headers,
    }: {
        resource: string;
        params?: string;
        method?: "GET" | "POST" | "PUT" | "DELETE";
        body?: any;
        headers?: any;
    }): Promise<any> {
        const url = `https://api.jwplayer.com/v2/sites/${process.env.NEXT_PUBLIC_JWPLAYER_SITE_ID}/${resource}/${params}`;
        const request = {
            method: method || "GET",
            headers: {
                Authorization: `Bearer ${process.env.NEXT_PUBLIC_JWPLAYER_API_KEY}`,
                ...headers,
            },
            body: body ? JSON.stringify(body) : undefined,
        };
        const query = await fetch(url, request).then((res) => res.json());
        return query;
    }
}
