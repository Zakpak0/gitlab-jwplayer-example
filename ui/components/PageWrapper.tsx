import TableOfContents from "../elements/TableOfContents";

export default function PageWrapper({ children }: any) {
    return (
        <div
            style={{
                display: "flex",
                flexDirection: "row",
            }}>
            <div
                style={{
                    display: "flex",
                    border: "solid 1px white",
                    height: "100vh",
                    borderRight: "solid 1px black",
                    boxShadow: "0px 0px 10px 0px rgba(0,0,0,0.75)",
                }}>
                <TableOfContents />
            </div>
            <div
                style={{
                    display: "flex",
                    height: "100vh",
                    flex: 1,
                    justifyContent: "center",
                }}>
                {children}
            </div>
        </div>
    );
}
