import Link from "next/link";
import PageWrapper from "../../ui/components/PageWrapper";

export default function PlaylistsTOC() {
    return (
        <PageWrapper>
            <div
                style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                }}>
                <h1>Playlists Player</h1>
                <p>Playlists player example</p>
                <Link href={"/playlists/autoplay-player"}>Autoplay Player</Link>
            </div>
        </PageWrapper>
    );
}
