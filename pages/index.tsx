import Link from "next/link";
import PageWrapper from "../ui/components/PageWrapper";

export default function Home() {
    return (
        <PageWrapper>
            <div
                style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                }}>
                <h1>JWPlayer Example</h1>
                <p>Table of Contents</p>
                <Link href={"/basic"}>Basic Examples</Link>
                <Link href={"/custom"}>Custom Examples</Link>
                <Link href={"/playlists"}>Playlists Examples</Link>
                <Link href={"/content-management"}>Content Management Examples</Link>
            </div>
        </PageWrapper>
    );
}
