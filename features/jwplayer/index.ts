import Utilities from "../../internals/utilities";

export default class JWPlayer {
    static SDK = class {
        static async getPlayers(params: any) {
            const query = await Utilities.jwPlayerRequest({
                resource: "players",
                params,
            });
            return query;
        }
    };
}
